from django.db import models



class Feed(models.Model):
    url = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.url}"


class Item(models.Model):
    title = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    description = models.CharField(max_length=2000)
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.title}\n{self.description[:5]}....."
