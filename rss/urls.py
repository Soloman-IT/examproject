from django.urls import path

from . import views


urlpatterns = [
    path('', views.main, name='main'),
    path('<int:feed_id>/item', views.item, name='item'),
    path('feed/', views.feed, name='feed')
]