from django.http import  Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.urls import reverse

from .models import Item, Feed

import requests
import xml.etree.ElementTree as ET


def item(request, feed_id):
    get_feed = get_object_or_404(Feed, pk=feed_id)
    items = get_list_or_404(Item, feed=get_feed)
    context = {'items': items}
    return render(request, "rss/item.html", context)


def feed(request):
    if request.method == "POST":
        link = request.POST["feed"]

        req = requests.get(link)

        uni_feed_lst = ET.fromstring(req.text).findall("channel/item")

        if len(uni_feed_lst) == 0:
            raise Http404("Убедитесь в правильности url")

        try:
            uni_feed = Feed.objects.get(url=link)
        except BaseException:
            uni_feed = None

        if uni_feed:
            list_item = Item.objects.all()

            for i in uni_feed_lst:
                for j in list_item:
                    if i.find("link").text == j.link:

                        j.title = i.find("title").text
                        j.description = i.find("description").text
                        j.save()
        else:
            uni_feed_ = Feed(url=link)
            uni_feed_.save()
            for elem in uni_feed_lst:
                elem_item = Item(title=elem.find("title").text, link=elem.find("link").text, description=elem.find("description").text, feed=uni_feed_)
                elem_item.save()

    return HttpResponseRedirect(reverse('main'))


def main(request):
    return render(request, "rss/main.html", {'list_link_feed': Feed.objects.all()})
